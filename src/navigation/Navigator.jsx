import { createNativeStackNavigator } from '@react-navigation/native-stack'
import Home from '../screens/Home'
import Profile from '../screens/Profile'
import Map from '../screens/Map'
import CameraScreen from '../screens/CameraScreen'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Icon from 'react-native-vector-icons/Ionicons'

const Stack = createNativeStackNavigator()
const TabStack = createBottomTabNavigator()

function ProfileNavigator () {
  return (
    <Stack.Navigator>
      <Stack.Screen name='Profile' component={Profile} />
      <Stack.Screen name='Map' component={Map} />
    </Stack.Navigator>
  )
}
function Navigator () {
  return (
    <TabStack.Navigator initialRouteName='Home'>
      <TabStack.Screen
        name='Home'
        component={Home}
        options={{
          tabBarIcon: ({ focused, color, size }) => {
            return <Icon name={focused ? 'home' : 'home-outline'} size={30} color={color} />
          }
        }}
      />
      <TabStack.Screen
        name='Bière'
        component={ProfileNavigator}
        options={{
          tabBarIcon: ({ focused, color, size }) => {
            return <Icon name={focused ? 'beer' : 'beer-outline'} size={30} color={color} />
          }
        }}
      />
      <TabStack.Screen
        name='Camera'
        component={CameraScreen}
        options={{
          tabBarIcon: ({ focused, color, size }) => {
            return <Icon name={focused ? 'camera' : 'camera-outline'} size={30} color={color} />
          }
        }}
      />
    </TabStack.Navigator>
  )
}

export default Navigator
