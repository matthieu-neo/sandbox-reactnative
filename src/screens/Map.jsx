import { Text, View, Button, StyleSheet } from 'react-native'

function Map ({ navigation }) {
  return (
    <View style={styles.container}>
      <Text>Map Screen</Text>
      <Button title='Aller à la carte' onPress={() => navigation.navigate('Home')} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default Map
